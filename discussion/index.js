function printName() {
	console.log('My name is Alex')
};

printName()

function declaredFunction() {
	console.log("Hello, world from declaredFunction()");
};

declaredFunction();

let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

declaredFunction = function() {
	console.log("Updated declaredFunction");
};

declaredFunction();

const constantFunction = function() {
	console.log("Initialized with const");
};

constantFunction();

{
	let localVar = "Alonzo Matheo";

	console.log(localVar);
}

let globalVar = "Aizaac Ellis";
console.log(globalVar);

function showNames() {
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();

function myNewFunction() {
	let name = "Jane";

	function nestedFunction() {
		let nestedName = "John";
		console.log(name);
	}

	nestedFunction();
};
// nestedFunction();
myNewFunction();

let globalName = "Joy";

function myNewFunction2() {
	let nameInside = "Kenzo";

	console.log(globalName);
};

myNewFunction2();

alert("Hello, world!");

function showSampleAlert() {
	alert("Hello, user1");
};

showSampleAlert();

let samplePrompt = prompt("Enter your name: ");

console.log(`Hello, ${samplePrompt}`);

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log(`Hello, ${firstName} ${lastName}!`)
	console.log("Welcome to my page!")
};

printWelcomeMessage();

/*
Function naming conventions
	-function names should be definitive of the task it will perform
	-avoid generic names to avoid confusion within the code
	-Avoid pointless and inappropriate function names
	-Name your functions following camel casing
	-Don't use JS reserved keywords
*/