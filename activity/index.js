console.log("Hello, World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

const getUserNameAgeLocation = function() {
	let fullName = prompt("Enter your Full Name: ");
	let age = prompt("Enter your age: ");
	let location = prompt("Enter your Location: ");

	console.log(`Hello, ${fullName}`);
	console.log(`You are ${age} years old`);
	console.log(`You live in ${location}`);
};

getUserNameAgeLocation();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//second function here:
const getTopBands = function() {
	let band1 = "Paramore";
	let band2 = "Calla Lily";
	let band3 = "Boyce Avenue";
	let band4 = "Rivermaya";
	let band5 = "All Time Low";

	console.log(`1. ${band1}`);
	console.log(`2. ${band2}`);
	console.log(`3. ${band3}`);
	console.log(`4. ${band4}`);
	console.log(`5. ${band5}`);
};
getTopBands(); 	

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

const getTopMovies = function() {
	let movie1 = "The God Father";
	let movie2 = "The God Father, Part II";
	let movie3 = "Shawshank Redemption";
	let movie4 = "To Kill A Mockingbird";
	let movie5 = "Psycho";


	console.log(`1. ${movie1}`);
	console.log(`Rotten Tomatoes Rating: 97%`);
	console.log(`2. ${movie2}`);
	console.log(`Rotten Tomatoes Rating: 96%`);
	console.log(`3. ${movie3}`);
	console.log(`Rotten Tomatoes Rating: 91%`);
	console.log(`4. ${movie4}`);
	console.log(`Rotten Tomatoes Rating: 93%`);
	console.log(`5. ${movie5}`);
	console.log(`Rotten Tomatoes Rating: 96%`);
};

getTopMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();